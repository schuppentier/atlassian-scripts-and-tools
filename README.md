# Atlassian Scripts and Tools

This is my personal collection of scripts and tools to help working with Atlassian software.

## Contents
### `atlassian-addon-updates`
A python script to generate Jira issues when new Addon updates are available. Tested with Jira and Confluence but should
work with anything that uses the UPM.

### `grouprenamer`
A shell script to rename groups in Jira and Confluence, as Atlassian has not yet implemented such a feature themselves.

### `jira_customize`
A shell script to update some Jira settings after cloning from aproduction instance to a testing instance. The following
actions are executed:

* Base URL is updated
* Webhook URLs are updated
* The Mailserver host is set
* The color scheme is updated
* App Links are deleted