# Atlassian Addon Update Checker

This script talks to the Universal Plugin Manager (UPM) of Atlassian Applications (Jira, Confluence, etc.) and checks
the installed addons for any available updates. If an update is found, the script checks the configured Jira instance,
if an issue for this Addon already exists and creates one if needed.

## Dependencies
* `requests`
* `packaging`
* `jira`

Any dependencies can be installed by running `pip install -r requirements.txt`

## Configuration
The script is configured in the file `config_upm.py`. A sample configuration is provided in `config_upm.py.sample`.
There are three main sections:

### credentials
This defines the credentials used to login to the applications. The script assumes that all applications use the same
login credentials.

### jiraServer
This defines the target Jira for the issue creation. You can define the project, issue type and components of the issue
to create. `targetCompoinents` is optional and takes a list of dicts in the format `{'name': 'COMPONENTNAME'}`.

### instancesToCheck
This is a list of the instances Base URLs (Jira, Confluence, Bitbucket) that are to be checked.