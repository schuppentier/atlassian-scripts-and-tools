#!/usr/bin/python

import requests
from packaging import version
from jira import JIRA
import config_upm as config
import re

USER = config.credentials['user']
PASSWORD = config.credentials['password']
JIRA_SERVER = config.jira_server['hostname']
TARGET_PROJECT = config.jira_server['target_project']
TARGET_ISSUETYPE = config.jira_server['target_issuetype']
TARGET_COMPONENTS = config.jira_server['target_components']
INSTANCES = config.instances_to_check
OBJECT_SCHEMA_ID = config.insight['object_schema_id']
IQL_TEMPLATE = config.insight['iql_template']


def issue_exists(addon_key, instance):
    # Get issues that match the addon key and that are still unresolved
    jql = 'project = {} AND issuetype = {} AND Instanz in iqlFunction("URL = \\"{}\\"") AND Add-ons IN iqlFunction("Addon-Key = {}") AND component in ({}) AND resolution = Unresolved'.format(TARGET_PROJECT, TARGET_ISSUETYPE, instance, addon_key, ', '.join(['"{}"'.format(component.get('name')) for component in TARGET_COMPONENTS]))
    result = jira.search_issues(jql)
    return result

def create_issue(addon_name, addon_key, available_version, instance):
    # Get Insight Object
    iql_addons = IQL_TEMPLATE.format(addon_key, instance)
    iql_instance = "URL = {}".format(instance)
    insight_objects_addons = requests.get(JIRA_SERVER + '/rest/insight/1.0/iql/objects',
                                          auth=(USER, PASSWORD),
                                          headers=headers,
                                          params={'objectSchemaId': OBJECT_SCHEMA_ID, 'iql': iql_addons}).json().get('objectEntries', [])
    insight_objects_instance = requests.get(JIRA_SERVER + '/rest/insight/1.0/iql/objects',
                                          auth=(USER, PASSWORD),
                                          headers=headers,
                                          params={'objectSchemaId': OBJECT_SCHEMA_ID, 'iql': iql_instance}).json().get('objectEntries', [])
    custom_field_insight_objects_addons = [{'key': entry.get('objectKey')} for entry in insight_objects_addons]
    custom_field_insight_objects_instance = [{'key': entry.get('objectKey')} for entry in insight_objects_instance]
    fields = {
        'project': TARGET_PROJECT,
        'issuetype': TARGET_ISSUETYPE,
        'summary': 'Neue Addon-Version von "{}"'.format(addon_name),
        'description': 'Bitte testen und installieren!',
        'components': TARGET_COMPONENTS,
        'customfield_20800': custom_field_insight_objects_addons,
        'customfield_20900': custom_field_insight_objects_instance,
        'customfield_19403': available_version.public,
    }
    return(jira.create_issue(fields))

def check_instance(url):
    # Get installed addons
    addons_list = requests.get(url + '/rest/plugins/1.0/', auth=(USER, PASSWORD), headers=headers).json()
    addon_keys_list = [addon.get('key', None) for addon in addons_list.get('plugins', [])]
    addon_count = len(addon_keys_list)
    # Loop through installed addons and check for available updates
    for idx, addon_key in enumerate(addon_keys_list, start=1):
        # Print a status line with a poor man's progress meter
        print('[{idx:>{padding}}/{addon_count}] Checking addon {addon_key}'.format(idx=idx,
                                                                                   addon_count=addon_count,
                                                                                   addon_key=addon_key,
                                                                                   padding=len(str(addon_count))))
        # GETs the JSON represenation of the Addon that is being checked
        installed_addon_json = requests.get(url + '/rest/plugins/1.0/' + addon_key + '-key', auth=(USER, PASSWORD),
                                            headers=headers).json()
        if not installed_addon_json.get('userInstalled', False):
            continue
        # GETs the JSON representation of the most recent Marketplace version of the addon that is being checked. The
        # try except is needed in case of custom addons that are not availabnle on the Atlassian Marketplace. Those
        # return a 401 HTTP error and thus throw a ValueError when trying to parse them as JSON.
        try:
            available_addon_json = requests.get(url + '/rest/plugins/1.0/available/' + addon_key + '-key',
                                                auth=(USER, PASSWORD), headers=headers).json()
        except ValueError:
            continue

        installed_version = version.parse(installed_addon_json.get('version', '0.0.0'))
        available_version = version.parse(available_addon_json.get('version', '0.0.0'))
        addon_name = installed_addon_json.get('name', addon_key)

        # If the currently installed version is smaller than the available version from the marketplace (aka there is
        # an update available) AND there does not already exist a Jira issue for this Addon, then create a new issue.
        # Else, add a comment with the new version.
        if installed_version < available_version:
            result = issue_exists(addon_key, url)
            if result:
                issue = result[0]
                old_version = version.parse(issue.fields.customfield_19403)
                if old_version < available_version:
                    jira.add_comment(issue, "Neue Version verfügbar: {}".format(available_version.public))
                    issue.update(fields={'customfield_19403': available_version.public})
                    print('Commented Jira issue for new version of addon ' + addon_name + ': ' + issue.permalink())
            else:
                issue = create_issue(addon_name, addon_key, available_version, url)
                print('Created Jira issue for addon ' + addon_name + ': ' + issue.permalink())


headers = {'X-Atlassian-Token': 'nocheck'}
jira_options = {
    'server': JIRA_SERVER,
}
jira = JIRA(options=jira_options, basic_auth=(USER, PASSWORD))

# Loop through the configured instances
for instance in INSTANCES:
    check_instance(instance)
